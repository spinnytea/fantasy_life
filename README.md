Fantasy Life is a Nintendo 3DS game. It has a crafting system.
When you want to go on a crafting adventure, it's nice to have an *easy to use* checklist.
This is that checklist.

# threads to pick up (TODO)

* UI updates
    * proofread references page (look like it was throw in at the last second)
    * the app is basically done, but the workflow of the 3 pages is weird
        * redesign search
        * redesign list
        * redesign adventure
    * searching for items, the animation is a bit weird
    * "clear list" when "you adventured"
        * add a button in the jumbotron? along with some other stats?
        * make it an option? (auto clear list upon completing adventure)
    * add a notes sections backed by local storage (so I don't need my notepad files)
    * add a hunt section? specifically a hit list (not generated from gathering) and specifically so we have them in locations
* code TODOs
* deploy script
    * copy minimal vendors (just angular min/bootstrap dist, not whole thing)
    * copy over min static/data, not pretty static/data
    * <code>cp -r static/* ../spinnytea.bitbucket.org/fantasy_life/.</code>
* database
    * item sort order (how are they sorted in game? so we can sort gathering and crafting lists by that instead of name)
    * scrape extra ingredients for recipes
    * fish drop (375 Coldwater Tuna)
    * create a second output? one without pretty-print (that'll save a ton of space)
    * item icons?
    * item categories?
    * links to location names, links to items (on the wiki)
    * predefined carts for crafting job levels (i.e. joq quests + 1 of each item learned at that job level)

# See Also

## The Game

[Fantasy Life on Nintendo](http://fantasylife.nintendo.com/)
[Fantasy Life on Wikipedia](https://en.wikipedia.org/wiki/Fantasy_Life)
[Fantasy Life on Level 5](http://level5ia.com/product/fantasy-life-na/)

## Supplemental References

[Reveria Explorer](http://www.reveriaexplorer.com/)
[Fantasy Life Database](http://fantasylife.info/)
[Wikia](http://fantasy-life.wikia.com/wiki/Fantasy_Life_Wiki)

## Data Sources

[Reveria Explorer](https://github.com/vivianho/ReveriaExplorer)
I wanted to add to their website, but oh man is it hard to get started.
<code>CoffeeScript</code> and <code>Ruby on Rails</code>, dynamic forms via <code>jQuery</code>.
I may go back in the future as something new to learn, but it's a higher bar than I want to jump over right now.

## WebWeb

[Markdown](https://daringfireball.net/projects/markdown/)

... and of course all everything in `package.json` and `bower.json`

## Project References

[Crafter Life](https://bitbucket.org/spinnytea/crafter_life)
[Poke Type](https://bitbucket.org/spinnytea/po_ke_type)
