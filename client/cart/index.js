'use strict';
var _ = require('lodash');
var cakeCore = require('../common/CakeCore');

module.exports = exports = angular.module('fantasy_life.cart', []);
module.exports.controller('fantasy_life.cart.controller', [
  '$scope', 'fantasy_life.common.data.service', 'fantasy_life.data.items',
  function($scope, userDataService, itemsPromise) {
    $scope.randomJumbotronNoun = cakeCore.random();
    $scope.itemSelections = userDataService.cart.values();

    $scope.saveList = function() {
      userDataService.cart.save();
      redoItemCounts();
    };
    $scope.deleteFromList = function(item) {
      userDataService.cart.remove(item);
      $scope.itemSelections = userDataService.cart.values();
      redoItemCounts();
    };
    $scope.clearList = function() {
      userDataService.cart.clear();
      $scope.itemSelections = userDataService.cart.values();
      redoItemCounts();
    };
    $scope.setCraftList = function(selection) {
      if(selection) {
        userDataService.adventure.current([selection]);
      } else {
        userDataService.adventure.current($scope.itemSelections);
      }
    };
    $scope.newAdventure = function() {
      userDataService.adventure.input({});
      userDataService.adventure.current($scope.itemSelections);
    };

    function redoItemCounts() {
      $scope.itemSelectionCount = _.size($scope.itemSelections);
      $scope.itemSelectionSum = _.sumBy($scope.itemSelections, 'count');
    }

    // after we have our data ready
    itemsPromise.then(function(items) {
      $scope.itemCache = _.keyBy(items, 'id');
      redoItemCounts();
    });
  }
]);
