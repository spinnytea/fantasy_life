'use strict';
const fs = require('fs');
const request = require('request');

/**
 * load and cache a web page
 *
 * if the file is stored locally, then return the contents of the file
 * if the file does not exist, then load it from the url, then save and return the contents
 *
 * @param filepath - the location of the file, preferably under the "raw" directory
 * @param url - the url to fall back on
 * @returns {Promise} resolve with the contents of the file, reject with any err
 */
module.exports = function(filepath, url) {
  return new Promise(function(resolve, reject) {
    fs.readFile(filepath, { encoding: 'utf8' }, function(err, data) {

      // if we have the data, great! we are done
      if(data) return resolve(data);

      // if not available, then load from the url and save it
      console.log('requesting:', url);
      let options = {
        url: url,
        headers: {
          'Cookie': 'region=NA',
        }
      };
      request(options, function(err, res, body) {
        if(err) return reject(err);
        fs.writeFile(filepath, body, { encoding: 'utf8' }, function(err) {
          if(err) return reject(err);
          // finally we have the result from the url
          resolve(body);
        });
      });

    });
  });
};
