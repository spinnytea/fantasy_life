'use strict';
var _ = require('lodash');

module.exports = angular.module('fantasy_life.common', [
  require('./search').name,
]);

module.exports.factory('fantasy_life.common.data.service', [
  'localStorageService',
  function(localStorageService) {
    var service = {};

    // simple pass-through for simple things
    service.set = localStorageService.set;
    service.get = localStorageService.get;
    service.clearAll = function() {
      // the cart is stored as a local object, so we need to clear it out manually
      service.cart.clear();
      localStorageService.clearAll();
    };

    service.search = {
      clear: function() {
        localStorageService.remove('search.name');
        localStorageService.remove('search.type');
      },
    };

    var userItemList = localStorageService.get('cart') || {};
    service.cart = {
      add: function(i) {
        var item = userItemList[i.id] = (userItemList[i.id] || { id: i.id });
        item.count = ((item.count || 0)+1);
        return localStorageService.set('cart', userItemList);
      },
      count: function(i) {
        var item = userItemList[i.id];
        if(!item) return null;
        return item.count;
      },
      remove: function(i) {
        delete userItemList[i.id];
        return localStorageService.set('cart', userItemList);
      },
      values: function() {
        return _.values(userItemList);
      },
      save: function() {
        return localStorageService.set('cart', userItemList);
      },
      clear: function() {
        userItemList = {};
        return localStorageService.set('cart', userItemList);
      },
    };

    service.adventure = {
      // list of { id, count }
      current: function(items) {
        if(items) {
          return localStorageService.set('adventure.current', items);
        } else {
          return localStorageService.get('adventure.current') || [];
        }
      },
      // map of id: { i1, i2, i3 }
      input: function(input) {
        if(input) {
          return localStorageService.set('adventure.input', input);
        } else {
          return localStorageService.get('adventure.input') || {};
        }
      },
      clear: function() {
        localStorageService.remove('adventure.current');
        localStorageService.remove('adventure.input');
      },
    };

    function createOptionGetter(key, defaultValue) {
      return function() {
        if(localStorageService.keys().indexOf(key)  !== -1) {
          return localStorageService.get(key);
        } else {
          return defaultValue;
        }
      };
    }
    function createOptionSetter(key) {
      return function(value) {
        return localStorageService.set(key, value);
      };
    }
    service.options = {
      showCompletedItems: {
        get: createOptionGetter('options.showCompletedItems', true),
        set: createOptionSetter('options.showCompletedItems'),
      }
    };

    return service;
  }
]);

module.exports.directive('jobIcon', [function() {
  return {
    restrict: 'A',
    scope: { item: '=jobIcon', limit: '@' },
    templateUrl: 'common/jobIcon.html',
  };
}]);
