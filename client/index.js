'use strict';

module.exports = exports = angular.module('fantasy_life', [
  // folders
  require('./adventure').name,
  require('./cart').name,
  require('./common').name,
  require('./search').name,
  require('./site').name,
  // files
  require('./data').name,
  require('./utils').name,
  // vendor
  'templates',
  'LocalStorageModule',
  'ngAnimate',
  'ngRoute'
]);

exports.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'site/home.html',
    controller: 'fantasy_life.site.home.controller'
  }).when('/search', {
    templateUrl: 'search/search.html',
    controller: 'fantasy_life.search.controller',
  }).when('/cart', {
    templateUrl: 'cart/cart.html',
    controller: 'fantasy_life.cart.controller',
  }).when('/adventure', {
    templateUrl: 'adventure/adventure.html',
    controller: 'fantasy_life.adventure.controller',
  }).when('/reference', {
    templateUrl: 'site/reference.html',
  }).when('/settings', {
    templateUrl: 'site/settings.html',
    controller: 'fantasy_life.site.settings.controller'
  }).otherwise({
    templateUrl: 'oops.html'
  });
}]);
exports.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('fantasy_life')
    .setNotify(false, true);
}]);
