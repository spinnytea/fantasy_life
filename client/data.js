'use strict';
var _ = require('lodash');

module.exports = exports = angular.module('fantasy_life.data', []);

exports.factory('fantasy_life.data', [
  '$q',
  'fantasy_life.data.lives',
  'fantasy_life.data.items',
  'fantasy_life.data.locations',
  'fantasy_life.data.monsters',
  'fantasy_life.data.ores',
  'fantasy_life.data.shops',
  'fantasy_life.data.trees',
  function($q, livesPromise, itemPromise, locationPromise, monsterPromise, orePromise, shopPromise, treePromise) {
    return $q.all({
      lives: livesPromise,
      items: itemPromise,
      locations: locationPromise,
      monsters: monsterPromise,
      ores: orePromise,
      shops: shopPromise,
      trees: treePromise,
    }).then(function(loaded) {
      loaded.items = _.keyBy(loaded.items, 'id');
      loaded.locations = _.keyBy(loaded.locations, 'id');
      loaded.monsters = _.keyBy(loaded.monsters, 'id');
      loaded.ores = _.keyBy(loaded.ores, 'id');
      loaded.shops = _.keyBy(loaded.shops, 'id');
      loaded.trees = _.keyBy(loaded.trees, 'id');
      return loaded;
    });
  }
]);

jsonResource('items');
jsonResource('locations');
jsonResource('monsters');
jsonResource('ores');
jsonResource('shops');
jsonResource('trees');
function jsonResource(type) {
  exports.factory('fantasy_life.data.' + type, [ '$q', function($q) {
    return $q(function(resolve, reject) {
      $.getJSON('data/' + type + '.json').done(resolve).fail(reject);
    });
  }]);
}

exports.factory('fantasy_life.data.lives', [ '$q', function($q) {
  return $q.resolve([
    'Paladin', 'Mercenary', 'Hunter', 'Magician',
    'Miner', 'Woodcutter', 'Angler', 'Cook',
    'Blacksmith', 'Carpenter', 'Tailor', 'Alchemist',
  ]);
}]);
