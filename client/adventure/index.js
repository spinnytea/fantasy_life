'use strict';
var _ = require('lodash');

module.exports = exports = angular.module('fantasy_life.adventure', []);
module.exports.controller('fantasy_life.adventure.controller', [
  '$scope', 'fantasy_life.common.data.service', 'bindKeys', 'fantasy_life.data',
  function($scope, userDataService, bindKeys, dataPromise) {
    $scope.sectionGatherOpen = true;
    $scope.sectionCraftOpen = true;
    $scope.options = {};
    _.keys(userDataService.options).forEach(function(key) {
      $scope.options[key] = userDataService.options[key].get();
      // Note: don't save the options if they are changed locally
    });
    bindKeys($scope, {
      'g': function() { $scope.sectionGatherOpen = !$scope.sectionGatherOpen; },
      'c': function() { $scope.sectionCraftOpen = !$scope.sectionCraftOpen; },
      // XXX we need a better hot key for this; label the hot key on the settings page
      's': function() { $scope.options.showCompletedItems = !$scope.options.showCompletedItems; },
    });

    var current = userDataService.adventure.current();
    $scope.userInput = userDataService.adventure.input();
    if(!current.length) {
      $scope.noCurrent = true;
      $scope.noItems = !_.size(userDataService.cart.values());
      return;
    }

    $scope.done = false;
    var dag;
    dataPromise.then(function(data) {
      $scope.data = data;
      dag = $scope.dag = setupCraftingDAG(data.items, current);
      $scope.craftInto = buildCraftInto(data.items, current);

      dag.list.forEach(function(dagItem) {
        // either save or restore the dagItem input
        if(dagItem.id in $scope.userInput) {
          dagItem.userInput = $scope.userInput[dagItem.id];
        } else {
          $scope.userInput[dagItem.id] = dagItem.userInput;
        }

        // create a fixed sort order for the craft section
        if(!dagItem.gatherable) {
          // put final final things at the very bottom
          // (it just looks cleaner)
          // (it also has a climactic effect)
          if(dagItem.final && dagItem.order === 1) {
            // always at the bottom
            dagItem.craftSortOrder = '100';
          } else {
            // sorted otherwise
            dagItem.craftSortOrder = '0' + pad(dagItem.topologicalOrder, 2);
          }
          dagItem.craftSortOrder += pad(data.lives.indexOf(data.items[dagItem.id].recipe.life), 2);
        }
      });

      // XXX when we respond to the user input, the UI seems to always be behind by one update
      // - ... $scope.$watch('userInput' ...
      // - I REALLY don't understand why
      // - but watching the dag list seens to be fine
      $scope.$on('destroy', $scope.$watch('dag.list', updateUI, true));
    });

    function updateUI() {
      $scope.locations = buildLocations($scope.data, dag);
      // $scope.levelReqs = adventure.buildLevelRequirements(items, dag);
      recalculateCounts($scope.data.items, current, dag);
      userDataService.adventure.input($scope.userInput);

      $scope.gatherProgress = calculateProgress(dag.list.filter(function(dagItem) { return dagItem.gatherable; }));
      $scope.craftProgress = calculateProgress(dag.list.filter(function(dagItem) { return !dagItem.gatherable; }));
      $scope.done = (_.sumBy(dag.list, 'remaining') === 0);

      // init the view with fully expanded regions
      if(!$scope.expandedRegions) {
        $scope.expandedRegions = {};
        _.values($scope.data.locations).forEach(function(l) {
          $scope.expandedRegions[l.region] = true;
        });
      }
    }

    function calculateProgress(list) {
      var progress = {};
      progress.total = _.sumBy(list, 'raw_total');
      progress.remaining = _.sumBy(list, 'remaining');
      progress.percent = 100 * ( progress.total - progress.remaining ) / progress.total;
      progress.display = Math.floor(progress.percent) + '%';
      return progress;
    }

    //
    //
    //

    $scope.selectedDagItem = null;
    $scope.selectedItem = null;
    $scope.select = function(dagItem) {
      if($scope.selectedDagItem) {
        // clean up the previous selection
        // (only to prep for a new one)
        $scope.selectedDagItem.selected = false;
      }

      if(_.isNumber(dagItem)) {
        // this is actually the item id
        dagItem = dag.map[dagItem];
      }

      if($scope.selectedDagItem !== dagItem) {
        // make a new selection
        $scope.selectedDagItem = dagItem;
        $scope.selectedItem = $scope.data.items[dagItem.id];
        dagItem.selected = true;
      } else {
        // clear the previous selection
        $scope.selectedDagItem = null;
        $scope.selectedItem = null;
      }
    };
    $scope.isSelected = function(dagItem) {
      return dagItem === $scope.selectedDagItem;
    };
  }
]);

module.exports.directive('trDagItem', [function() {
  return {
    replace: true,
    scope: {
      data: '=', // whole database (data.items, data.trees, etc)
      selectCallback: '=', // a when we want to select or deselect and item
      isSelectedCallback: '=', // a when we want to select or deselect and item

      dagItem: '=',
      item: '='
    },
    templateUrl: 'adventure/dagItem.html',
    controller: ['$scope', DagItemController]
  };

  function DagItemController($scope) {
    var item = $scope.item;

    if(item.recipe) {
      $scope.materialNameList = item.recipe.ingredients
        .map(function(r) { return $scope.data.items[r.id].name; })
        .join(', ');
    }

    if($scope.dagItem.gather) {
      $scope.gatherNameList = $scope.dagItem.gather
        .map(function(g) { if(g.type === 'fishes') return { type: 'locations', id: g.id }; return g; })
        .map(function(g) { return $scope.data[g.type][g.id].name; })
        .join(', ');
    }

    if(item.monsters) {
      $scope.monsterNameList = item.monsters
        .map(function(m) { return $scope.data.monsters[m].name; })
        .join(', ');
    }

    if(item.shops) {
      $scope.shopNameList = item.shops
        .map(function(s) { return $scope.data.shops[s].name; })
        .join(', ');
    }
  }
}]);

module.exports.directive('selectedItem', [function() {
  return {
    replace: true,
    scope: {
      dagItem: '=',
      item: '=',

      items: '=',
      craftInto: '=',
      selectCallback: '='
    },
    templateUrl: 'adventure/selectedItem.html',
    controller: ['$scope', SelectedItemController]
  };

  function SelectedItemController($scope) {
    $scope.$on('destroy', $scope.$watch('item', function(item) {
      if(item && $scope.craftInto && $scope.items) {
        $scope.craftIntoList = buildLists([], item);
      } else {
        $scope.craftIntoList = undefined;
      }
    }));

    function buildLists(list, item) {
      list.push(item);
      var cis = $scope.craftInto[item.id];
      if(!cis) return [list];

      return _.flatten(cis.map(function(ci) {
        return buildLists(list.slice(), $scope.items[ci]);
      }));
    }
  }
}]);


// list is { id, count }
function setupCraftingDAG(allItems, list) {
  var dag = {
    map: {}
  };

  function traverse(listItem, order) {
    var id = listItem.id;
    var curr = dag.map[id];
    var item = allItems[id];
    if(!curr) {
      curr = dag.map[id] = {
        id: id,
        name: item.name,
        order: 0, // depth from selected
        final: false,
        total: 0,
        remaining: 0,
        userInput: {
          obtained: 0,
          action: undefined
        }
      };

      // make locations a non-enumerable property
      // this way we can update it without triggering the $watch on dag.list
      Object.defineProperty(curr, 'locations', { value: [] });
    }
    curr.order = Math.max(curr.order, order);

    if(item.recipe) {
      curr.gatherable = false;
      curr.userInput.action = 'craft';
      item.recipe.ingredients.forEach(function(i) {
        traverse({ id: i.id }, order+1);
      });

      // still show the locations you can find the item
      if(item.locations)
        curr.gather = item.locations.map(function(l) { return { type: 'locations', id: l.id, name: l.type }; });

    } else {
      curr.gatherable = true;

      if(item.trees || item.ores || item.fishes || item.locations) {
        curr.userInput.action = 'gather';
        curr.gather = _.flatten([
          (item.locations||[]).map(function(l) { return { type: 'locations', id: l.id, name: l.type }; }),
          (item.fishes||[]).map(function(f) { return { type: 'fishes', id: f }; }),
          (item.trees||[]).map(function(t) { return { type: 'trees', id: t }; }),
          (item.ores||[]).map(function(o) { return { type: 'ores', id: o }; }),
        ]);
      } else if(item.monsters) {
        curr.userInput.action = 'hunt';
      } else if(item.shops) {
        curr.userInput.action = 'shop';
      }
    }
  }
  list.forEach(function(li) { traverse(li, 1); });
  list.forEach(function(li) { dag.map[li.id].final = true; });

  dag.list = _.values(dag.map);
  dag.list = _.sortBy(dag.list, 'order');

  // create a topological order
  var topologicalOrder = 1; // start at leaves and work towards selected
  var moreLevels = true;
  // start with non-craftable items
  dag.list.forEach(function(dagItem) { if(!allItems[dagItem.id].recipe) dagItem.topologicalOrder = topologicalOrder; });
  while(moreLevels) {
    moreLevels = false;
    topologicalOrder++;
    // as a first pass, mark everything on this level
    dag.list.forEach(function(dagItem) {
      if(!dagItem.hasOwnProperty('topologicalOrder')) {
        var isLeaf = allItems[dagItem.id].recipe.ingredients.every(function(ingredient) {
          return dag.map[ingredient.id].topologicalOrder;
        });
        if(isLeaf) {
          dagItem.topologicalOrder = null;
          moreLevels = true;
        }
      }
    });
    // apply the topologicalOrder to the leaves
    dag.list.forEach(function(dagItem) {
      if(dagItem.topologicalOrder === null)
        dagItem.topologicalOrder = topologicalOrder;
    });
  }

  recalculateCounts(allItems, list, dag);

  return dag;
}

function recalculateCounts(allItems, list, dag) {
  // reset the totals
  // - totals are used for current state
  // - raw_totals are only used for progress
  dag.list.forEach(function(dagItem) { dagItem.total = dagItem.raw_total = 0; });
  // set the seed counts
  list.forEach(function(listItem) { dag.map[listItem.id].total = dag.map[listItem.id].raw_total = listItem.count; });
  // do the topological traversal (follow the ordered sort; most complex item to least complex item)
  dag.list.forEach(function(dagItem) {
    // by this point the current value is the total we need for THIS ITEM
    // so we can use it as a multiplier for its dependencies
    dagItem.remaining = Math.max(0, dagItem.total - dagItem.userInput.obtained);

    if(allItems[dagItem.id].recipe) {
      var material_multiplier = 0;
      var raw_multiplier = 0;
      // if we aren't crafting, then we don't need to go deeper
      if(dagItem.userInput.action === 'craft')
        material_multiplier = dagItem.remaining;
      raw_multiplier = dagItem.raw_total;
      allItems[dagItem.id].recipe.ingredients.forEach(function(i) {
        dag.map[i.id].total += i.quantity * material_multiplier;
        dag.map[i.id].raw_total += i.quantity * raw_multiplier;
      });
    }
  });
}

function buildLocations(data, dag) {
  var locations = {};

  function getRegionList(loc) {
    var region = (locations[loc.region] = (locations[loc.region] || {}));
    return (region[loc.id] = (region[loc.id] || []));
  }

  function singleLocation(id, name, job, dagItem) {
    var loc = {
      job: job,
      dagItem: dagItem,
      loc: data.locations[id],
      name: name,
    };
    getRegionList(loc.loc).push(loc);
    return loc;
  }
  function manyLocations(thing, job, dagItem) {
    return thing.locations.map(function(loc) { return data.locations[loc]; }).map(function(l) {
      var loc = {
        job: job,
        dagItem: dagItem,
        loc: l,
        name: thing.name,
      };
      getRegionList(loc.loc).push(loc);
      return loc;
    });
  }

  var list = dag.list.filter(function(dagItem) { return dagItem.remaining > 0; });
  list.forEach(function(dagItem) {
    var item = data.items[dagItem.id];

    // locations is readonly, so we need to update splice the new data into it
    var newDagItemLocations = [];

    if(dagItem.userInput.action === 'gather') {
      newDagItemLocations = _.flatten(dagItem.gather.map(function(g) {
        switch(g.type) {
          case 'locations':
            return [singleLocation(g.id, g.name, { locations: true }, dagItem)];
          case 'fishes':
            return [singleLocation(g.id, 'Fish', { fishes: true }, dagItem)];
          case 'trees':
            var tree = data.trees[g.id];
            return manyLocations(tree, { trees: [tree] }, dagItem);
          case 'ores':
            var ore = data.ores[g.id];
            return manyLocations(ore, { ores: [ore] }, dagItem);
        }
      }));
    }

    if(dagItem.userInput.action === 'hunt') {
      newDagItemLocations = _.flatten(item.monsters.map(function(m) { return data.monsters[m]; }).map(function(m) {
        return manyLocations(m, { monsters: [m] }, dagItem);
      }));
    }

    if(dagItem.userInput.action === 'shop') {
      newDagItemLocations = item.shops.map(function(s) { return data.shops[s]; }).map(function(s) {
        return singleLocation(s.location, s.name, { shops: [s] }, dagItem);
      });
    }

    dagItem.locations.splice(0);
    Array.prototype.push.apply(dagItem.locations, newDagItemLocations);
  });

  // create a list of names
  // this makes something that's ordered and iterable for the UI
  _.forEach(locations, function(region) { region.$ids = _.keys(region).sort(); });
  // TODO we need a better order for the region names
  // - how about the min loc id?
  locations.$names = _.keys(locations).sort();

  return locations;
}

// build an inverse mapping of recipe ingredients
// only include final items
function buildCraftInto(allItems, list) {
  var craftInto = {};

  list = _.map(list, 'id');
  var next = list.filter(function(id) { return allItems[id].recipe; });

  while(next.length) {
    list = next.splice(0);
    list.forEach(function(id) {
      var item = allItems[id];
      // map the ingredients
      item.recipe.ingredients.forEach(function(ingredient) {
        (craftInto[ingredient.id] = (craftInto[ingredient.id] || [])).push(item.id);
        // check to see if the ingredient has a recipe
        if(allItems[ingredient.id].recipe) next.push(ingredient.id);
      });
    });
  }

  // unique the list (not sure why this is happening)
  _.keys(craftInto).forEach(function(k) { craftInto[k] = _.uniq(craftInto[k]); });

  return craftInto;
}

function pad(num, size) {
  var s = '000000000' + num;
  return s.substr(s.length-size);
}
