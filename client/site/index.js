'use strict';
var _ = require('lodash');
// some high-level stuff
// manages the overall site

module.exports = exports = angular.module('fantasy_life.site', []);
function isDay() {
  var h = new Date().getHours();
  return (6 < h && h < 18);
}

var DEFAULT_THEME = (isDay() ? 'journal' : 'darkly');
exports.controller('fantasy_life.site.html.controller', [
  '$rootScope', '$location', 'fantasy_life.common.data.service',
  function SiteHtmlController($rootScope, $location, userDataService) {
    var controller = this;
    controller.theme = userDataService.get('theme') || DEFAULT_THEME;
    $rootScope.$on('LocalStorageModule.notification.removeitem', function(event, args) {
      if(args.key === 'theme') {
        controller.theme = DEFAULT_THEME;
      }
    });

    $rootScope.$watch(function() { return $location.path(); }, function(path) {
      controller.locationClass = ('' || path).replace(/^\//, '') || 'home';

      controller.locationClass += (isDay() ? ' day' : ' night');
    });
  }
]);

exports.directive('pageHeader', [
  function() {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'site/pageHeader.html',
      controller: [
        '$scope', '$location',
        PageHeaderController
      ]
    };

    function PageHeaderController($scope, $location) {
      $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
      };

      // bugfix: force the dropdown to close
      // XXX this works on the desktop, but it doesn't work on mobile
      $('.navbar .navbar-nav a').click(function() {
        var navbar_toggle = $('.navbar-toggle');
        if (navbar_toggle.is(':visible')) {
          navbar_toggle.trigger('click');
        }
      });
    }
  }
]);

exports.controller('fantasy_life.site.home.controller', [
  '$scope', 'fantasy_life.data', 'fantasy_life.common.data.service',
  function SiteHomeController($scope, dataPromise, userDataService) {
    $scope.hasCart = !!_.size(userDataService.cart.values());
    $scope.hasAdventure = !!userDataService.adventure.current().length;

    dataPromise.then(function(loaded) {
      _.keys(loaded).forEach(function(key) {
        $scope[key + 'Count'] = _.values(loaded[key]).length;
      });
      $scope.lives = loaded.lives;
      $scope.recipeCount = _.values(loaded.items).filter(function(item) {
        return ('recipe' in item);
      }).length;
    });
  }
]);

exports.controller('fantasy_life.site.settings.controller', [
  '$scope', 'fantasy_life.common.data.service',
  function SiteSettingsController($scope, userDataService) {
    // get themes from server
    $scope.themes = [ $scope.html.theme ]; // seed the list so the user doesn't notice
    $.getJSON('data/themes.json').done(function(list) { $scope.themes = list; $scope.$digest(); }); // fill in the full list once we have it
    $scope.saveTheme = function() {
      userDataService.set('theme', $scope.html.theme);
    };

    $scope.options = {};
    _.keys(userDataService.options).forEach(loadOption);
    function loadOption(key) {
      $scope.options[key] = userDataService.options[key].get();
      $scope.$on('$destroy', $scope.$watch('options.' + key, function(newValue, oldValue) {
        if(newValue !== oldValue) {
          userDataService.options[key].set(newValue);
        }
      }));
    }

    $scope.clearSearch = function() {
      userDataService.search.clear();
    };
    $scope.clearCart = function() {
      userDataService.cart.clear();
    };
    $scope.clearAdventure = function() {
      userDataService.adventure.clear();
    };
    $scope.clearLocalStorage = function() {
      userDataService.clearAll();
      _.keys(userDataService.options).forEach(function(key) {
        $scope.options[key] = userDataService.options[key].get();
      });
    };
  }
]);
