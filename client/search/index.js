'use strict';

module.exports = exports = angular.module('fantasy_life.search', []);
module.exports.controller('fantasy_life.search.controller', [
  '$scope', '$q', 'fantasy_life.common.search', 'fantasy_life.data.items', 'fantasy_life.common.data.service',
  function($scope, $q, Search, itemsPromise, userDataService) {
    $scope.itemFormModel = {
      name: userDataService.get('search.name'),
      type: userDataService.get('search.type') || 'fuzzy'
    };

    $scope.addToItemList = function(item) {
      return userDataService.cart.add(item);
    };
    $scope.getItemListCount = function(item) {
      return userDataService.cart.count(item);
    };

    // this is the search object
    $scope.search = new Search(filteredItems, 'name');
    function filteredItems(params) {
      return itemsPromise.then(function(list) {
        return $q(function(resolve, reject) {
          setTimeout(function() {
            switch(params.type) {
              case 'index': resolve(indexFilter(params, list)); break;
              case 'fuzzy': resolve(fuzzyFilter(params, list)); break;
              case 'regex': resolve(regexFilter(params, list)); break;
              default: return reject();
            }
          }, 300);
        });
      });
    }

    function indexFilter(params, list) {
      var name = params.name.toLowerCase();
      return list.filter(function(item) {
        item.$rank = -item.name.toLowerCase().indexOf(name);
        return item.$rank !== 1;
      });
    }
    function fuzzyFilter(params, list) {
      return textSearch(list, params.name);
    }
    function regexFilter(params, list) {
      var regex = new RegExp(params.name, 'i');
      return list.filter(function(item) {
        var match = item.name.match(regex);
        if(match) item.$rank = -item.name.indexOf(match[0]);
        return !!match;
      });
    }

    $scope.$on('destroy', $scope.$watch('itemFormModel.name', doSearch));
    $scope.$on('destroy', $scope.$watch('itemFormModel.type', doSearch));
    function doSearch() {
      var params = {
        hasRecipe: true,
        hasGather: true,
        name: $scope.itemFormModel.name,
        type: $scope.itemFormModel.type,
      };

      userDataService.set('search.name', params.name);
      userDataService.set('search.type', params.type);

      if(params.name && params.name.replace(/\\\w|[.*]/g, '').length > 1) {
        $scope.search.go(params);
      } else {
        $scope.search.reset();
      }
    }
  }
]);

var FuzzySearch = require('fuzzysearch-js');
var levenshteinFS = require('fuzzysearch-js/js/modules/LevenshteinFS');
var indexOfFS = require('fuzzysearch-js/js/modules/IndexOfFS');
var sift3FS = require('fuzzysearch-js/js/modules/Sift3FS');
//var wordCountFS = require('fuzzysearch-js/js/modules/WordCountFS');
function textSearch(items, term) {
  // XXX I have no idea what any of this configuration is for
  // it's copy-pasta from the example
  var fuzzySearch = new FuzzySearch(items, {'minimumScore': 300, 'termPath': 'name', 'caseSensitive': false});
  fuzzySearch.addModule(levenshteinFS({'maxDistanceTolerance': 3, 'factor': 3}));
  fuzzySearch.addModule(indexOfFS({'minTermLength': 3, 'maxIterations': 500, 'factor': 3}));
  fuzzySearch.addModule(sift3FS({'maxDistanceTolerance': 3, 'factor': 1}));
  //fuzzySearch.addModule(wordCountFS({'maxWordTolerance': 3, 'factor': 1}));

  return fuzzySearch.search(term).map(function(r) {
    r.value.$rank = r.score;
    return r.value;
  });
}
