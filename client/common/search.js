'use strict';
var _ = require('lodash');

module.exports = exports = angular.module('fantasy_life.common.search', []);

exports.factory('fantasy_life.common.search', [
  function() {
    function Search(searchFn, matchProp) {
      this.status = {
        hasSearched: false,
        isSearching: false,
        hasError: false
      };
      this.results = {
        list: []
      };

      this.searchFn = searchFn;
      this.matchProp = matchProp;
    }

    Search.prototype.go = function(queryParams) {
      this.status.isSearching = true;
      this.searchFn(queryParams).then(this.setSearchResults.bind(this), (function() {
        this.status.hasSearched = true;
        this.status.isSearching = false;
        this.status.hasError = true;
      }).bind(this));
    };

    Search.prototype.setSearchResults = function(list) {

      // ngAnimate tracks objects; we can't switch out the object
      // so use the object from the last search when we can
      var oldResults = _.keyBy(this.results.list, this.matchProp);
      var self = this;
      this.results.list = list.map(function(obj) {
        var old = oldResults[obj[self.matchProp]];
        if(old) {
          // TODO allow for custom special keys
          // XXX should we merge the old and the new, omitting $$hashKey
          // update the old object's rank with the new rank
          old.$rank = obj.$rank;
          return old;
        }
        return obj;
      });

      this.status.hasSearched = true;
      this.status.isSearching = false;
      this.status.hasError = false;
      this.hasMoreResults = (list.status === 206);
    };

    Search.prototype.reset = function() {
      this.results.list = [];
      this.status.hasSearched = false;
      this.status.isSearching = false;
      this.status.hasError = false;
    };

    Search.prototype.hasResults = function() {
      return this.results.list.length;
    };

    return Search;
  }
]);

exports.directive('searchMessages', [
  function SearchMessagesDirective() {
    return {
      scope: { search: '=searchMessages' },
      templateUrl: 'common/searchMessages.html',
      controller: [
        '$scope',
        SearchMessagesController
      ]
    };

    function SearchMessagesController($scope) {
      var status = $scope.search.status;

      $scope.showSearchingMessage = function() {
        return status.isSearching && !status.hasSearched;
      };

      $scope.showErrorMessage = function() {
        return !status.isSearching && status.hasError;
      };

      $scope.showFirstSearchMessage = function() {
        return !status.isSearching && !status.hasError && !status.hasSearched;
      };

      $scope.showNoResultsMessage = function() {
        return !status.isSearching && !status.hasError && status.hasSearched &&
          ($scope.search.results.list.length === 0);
      };
    }
  }
]);
