'use strict';
const _ = require('lodash');
const bluebird = require('bluebird');
const cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');
const lp = require('./load_page');

const OUTPUT_MIN = false;
const TYPES = ['locations', 'shops', 'monsters', 'trees', 'ores', 'items'];
const PATCH = ['locations', 'items'];
const MAX = {
  locations: 89,
  shops: 77,
  monsters: 265,
  trees: 36,
  ores: 57,
  items: 1721,
};
const PROCESS = {};
const REVERIA_URL = 'http://www.reveriaexplorer.com/';

exports.process = function(path_to_raw, path_to_data) {
  var state = {
    number: undefined,
    type: undefined,
    raw: path_to_raw,
  };

  let promise = TYPES.reduce(function(promise, type) {
    return promise.then(function() {
      state.number = 1;
      state.type = type;
      state[type] = [];
      return next(state);
    }).then(function() {
      // TODO instead of using a list, check to see if the file exists
      if(_.includes(PATCH, type)) {
        // TODO this path is a hack; we should specify the path elsewhere
        return loadjson(path.join(path_to_raw, '..', '..', 'patch', type + '.json')).then(function(p) {
          let index = _.keyBy(state[type], 'id');
          p.forEach(function(o) { _.merge(index[o.id], o); });

          // XXX make it generic when there's more uses
          if(type === 'locations') {
            _.forEach(index, function(o) {
              if(!o.town) delete o.town; // clean up the data
              if(!o.region) console.log('missing region', o.id, o.name); // XXX reject?
            });
          }
        });
      }
    }).then(function() {
      return savejson(state[type], path.join(path_to_data, type + '.json'));
    });
  }, Promise.resolve());

  return promise.then(function() {
    return 'finished scraping reveria';
  });
};

function next(state) {
  if(state.number === 1) console.log('...processing', state.type);

  return lp(path.join(state.raw, state.type, state.number + '.html'), REVERIA_URL + state.type + '/' + state.number).then(function(data) {
    if(PROCESS[state.type]) {
      let message = PROCESS[state.type](state, data);
      if(message) return Promise.reject('Error while parsing ' + state.type + '/' + state.number + ' ' + message);
    }

    if(state.number < MAX[state.type]) {
      state.number++;
      return next(state);
    }
  });
}

PROCESS.locations = function processLocation(state, data) {
  // this location doesn't exist in the reveria database
  if(state.number === 37) return;

  const $ = cheerio.load(data);
  const location = {
    id: state.number
  };

  let name = $('h1#name');
  if(name.length !== 1) return 'name';
  location.name = name.text();

  let town = $('h2#town');
  if(town.length !== 1) return 'town';
  location.town = town.text();
  if(!location.town) delete location.town;

  state.locations.push(location);
};

PROCESS.shops = function processShop(state, data) {
  // this shop doesn't exist in the reveria database
  if(state.number === 30) return;

  const $ = cheerio.load(data);
  const shop = {
    id: state.number
  };

  let name = $('h1#Name');
  if(name.length !== 1) return 'name';
  shop.name = name.text();

  let location = $('strong:contains("Location")+a');
  if(name.length !== 1) return 'location';
  shop.location = location.attr('href');
  if(shop.location.indexOf('/locations/') !== 0) return 'location id.';
  shop.location = +shop.location.replace('/locations/', '');
  if(!_.find(state.locations, { id: shop.location, name: location.text() })) return 'cannot find location';

  state.shops.push(shop);
};

PROCESS.monsters = function processMonster(state, data) {
  // this monster doesn't exist in the reveria database
  if(_.includes([1, 2, 22, 23, 90, 118, 144], state.number)) return;

  const $ = cheerio.load(data);
  const monster = {
    id: state.number
  };

  let name = $('h1');
  if(name.length !== 1) return 'name';
  monster.name = name.text();

  let locations = $('h3:contains("Locations")+ul');
  if(locations.length !== 1) return 'no location';
  monster.locations = [];
  $('h3:contains("Locations")+ul li').each(function(i, elem) {
    elem = $(elem);
    let location = {
      id: +elem.find('a').attr('href').replace('/locations/', ''),
      name: elem.find('a').text().trim(),
    };

    if(!_.find(state.locations, { id: location.id, name: location.name })) console.log('cannot find location');

    monster.locations.push(location.id);
  });

  state.monsters.push(monster);
};

PROCESS.trees = function processTree(state, data) {
  // this tree doesn't exist in the reveria database
  if(_.includes([7, 30], state.number)) return;

  const $ = cheerio.load(data);
  const tree = {
    id: state.number
  };

  let name = $('h1');
  if(name.length !== 1) return 'name';
  tree.name = name.text();

  let locations = $('h3:contains("Locations")+ul');
  if(locations.length !== 1) return 'no location';
  tree.locations = [];
  $('h3:contains("Locations")+ul li').each(function(i, elem) {
    elem = $(elem);
    let location = {
      id: +elem.find('a').attr('href').replace('/locations/', ''),
      name: elem.find('a').text().trim(),
    };

    if(!_.find(state.locations, { id: location.id, name: location.name })) console.log('cannot find location');

    tree.locations.push(location.id);
  });

  state.trees.push(tree);
};

PROCESS.ores = function processOre(state, data) {
  // this ore doesn't exist in the reveria database
  if(_.includes([1, 2, 3, 18], state.number)) return;

  const $ = cheerio.load(data);
  const ore = {
    id: state.number
  };

  let name = $('h1');
  if(name.length !== 1) return 'name';
  ore.name = name.text();

  let locations = $('h3:contains("Locations")+ul');
  if(locations.length !== 1) return 'no location';
  ore.locations = [];
  $('h3:contains("Locations")+ul li').each(function(i, elem) {
    elem = $(elem);
    let location = {
      id: +elem.find('a').attr('href').replace('/locations/', ''),
      name: elem.find('a').text().trim(),
    };

    if(!_.find(state.locations, { id: location.id, name: location.name })) console.log('cannot find location');

    ore.locations.push(location.id);
  });

  state.ores.push(ore);
};

// TODO try throw the message instead of returning it
// TODO normalize data (or at least, only store IDs for things that have their own array)
PROCESS.items = function processItem(state, data) {
  // this item doesn't exist in the reveria database
  if(_.includes([996, 997, 998, 999], state.number)) return;

  // these items exist, but they are 'super' versions of normal items
  // - they aren't different items in the game, but they are in this database
  // - I can't find them in other databases either
  // - the ones that are commented out have gatherable locations (Superior Campfire Roast)
  if(_.includes([394, 396, 397, /*399, 402,*/ 403, /*1112,*/ 1127, /*1558*/], state.number)) return;

  const $ = cheerio.load(data);
  const item = {
    id: state.number
  };

  let name = $('h1');
  if(name.length !== 1) return 'name';
  item.name = name.text();
  if(item.name === 'The page you were looking for doesn\'t exist.')
    // console.log(item.id);
    return; // XXX instead of skipping the item by name, we should vet the item in the list above

  // let description = $('th:contains("Description")+td');
  // if(name.length !== 1) return 'description';
  // item.desc = description.text().trim();

  if($('h3:contains("Recipe Info")').length === 1) {
    item.recipe = {
      life: $('h3:contains("Recipe Info")+div th:contains("Life")+td').text().trim(),
      rank: $('h3:contains("Recipe Info")+div th:contains("Ranking")+td').text().trim().split('\n')[0],
      ingredients: [],
    };

    // ingredients table
    let in_tab = $('h3:contains("Recipe Info")+div+div table');
    if(in_tab.length !== 1) return 'ingredients table';
    in_tab.find('tr').each(function(i, elem) {
      elem = $(elem);
      // TODO check headers
      if(i > 0) {
        // TODO matcher on href
        // TODO check recipe item name after list is built
        item.recipe.ingredients.push({
          id: +elem.find('a').attr('href').replace('/items/', ''),
          // name: elem.find('a').text().trim(),
          quantity: +elem.find('td+td').text().trim(),
        });
      }
    });

    if(!item.recipe.life) return 'recipe life';
    if(!item.recipe.rank) return 'recipe rank';
    // XXX only one recipe has no ingredients
    // if(!item.recipe.ingredients.length) return 'recipe ingredients';
  }

  if($('h3:contains("Shops")').length === 1) {
    item.shops = [];
    // TODO error checking on fields
    $('h3:contains("Shops")+div>table tr').each(function(i, elem) {
      elem = $(elem);
      if(i > 0) {
        let shop = {
          id: +elem.find('a').attr('href').replace('/shops/', ''),
          name: elem.find('a').text().trim(),
          cost: +elem.find('td+td').text().trim(),
        };

        let match = _.find(state.shops, { id: shop.id, name: shop.name });
        if(!match) console.log('Cannot find shop:', shop.id, shop.name);

        item.shops.push(shop);
      }
    });

    // only 3 items that vary in cost; do I need to do anything about it?
    // let costs = _.uniq(_.map(item.shops, 'cost'));
    // if(costs.length !== 1) return 'Prices may vary.';

    item.cost = _.max(_.map(item.shops, 'cost'));
    item.shops = _.map(item.shops, 'id'); // since we have a cost
  }

  if($('h3:contains("Gathering Locations")').length === 1) {
    item.locations = [];
    $('h3:contains("Gathering Locations")+div>table tr').each(function(i, elem) {
      elem = $(elem);
      if(i > 0) {
        let location = {
          id: +elem.find('a').attr('href').replace('/locations/', ''),
          name: elem.find('a').text().trim(),
          type: elem.find('td+td').text().trim(),
        };

        if(!_.find(state.locations, { id: location.id, name: location.name })) console.log('cannot find location');
        delete location.name;

        item.locations.push(location);
      }
    });
  }

  if($('h3:contains("Monster Drops")').length === 1) {
    item.monsters = [];
    $('h3:contains("Monster Drops")+ul li').each(function(i, elem) {
      elem = $(elem);
      let monster = {
        id: +elem.find('a').attr('href').replace('/monsters/', ''),
        name: elem.find('a').text().trim(),
      };

      if(!_.find(state.monsters, { id: monster.id, name: monster.name })) console.log('cannot find monster');
      delete monster.name;

      item.monsters.push(monster.id);
    });
  }

  if($('h3:contains("Tree Drops")').length === 1) {
    item.trees = [];
    $('h3:contains("Tree Drops")+ul li').each(function(i, elem) {
      elem = $(elem);
      let tree = {
        id: +elem.find('a').attr('href').replace('/trees/', ''),
        name: elem.find('a').text().trim(),
      };

      if(!_.find(state.trees, tree)) console.log('cannot find tree');

      item.trees.push(tree.id);
    });
  }

  if($('h3:contains("Ore Drops")').length === 1) {
    item.ores = [];
    $('h3:contains("Ore Drops")+ul li').each(function(i, elem) {
      elem = $(elem);
      let ore = {
        id: +elem.find('a').attr('href').replace('/ores/', ''),
        name: elem.find('a').text().trim(),
      };

      if(!_.find(state.ores, ore)) console.log('cannot find ore');

      item.ores.push(ore.id);
    });
  }

  if($('h3:contains("Fish Info")').length === 1) {
    item.fishes = [];
    let list = $('h3:contains("Fish Info")+div table th:contains("Location(s)")+td');
    if(list.length !== 1) return 'cannot find fish info location';
    list.find('a').each(function(i, elem) {
      elem = $(elem);
      let location = {
        id: +elem.attr('href').replace('/locations/', ''),
        name: elem.text().trim(),
      };

      if(!_.find(state.locations, { id: location.id, name: location.name })) console.log('cannot find location');

      item.fishes.push(location.id);
    });
  }

  state.items.push(item);
};

function loadjson(file) {
  return new Promise(function(resolve, reject) {
    fs.readFile(file, { encoding: 'utf8' }, function(err, str) {
      if(err) return reject(err);
      resolve(JSON.parse(str));
    });
  });
}
function savejson(obj, file) {
  return new Promise(function(resolve, reject) {
    let str = (OUTPUT_MIN?JSON.stringify(obj):JSON.stringify(obj, null, 2));
    fs.writeFile(file, str, { encoding: 'utf8' }, function(err) {
      if(err) return reject(err);
      resolve();
    });
  });
}
